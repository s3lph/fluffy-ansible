# Ansible Playbook For My Home Router

This repository contains Ansible roles to configure my home router, a PCEngines APU2 running Arch Linux.
The host variables are managed in a separate, private repository.  An example host_vars configuration
can be found in the root directory of the repository.

License: MIT

## Roles

- systemd-networkd config
- ISC DHCPD v4+v6
- ISC BIND9 (root zone slave)
- radvd IPv6 Router Advertisement
- iptables
- hostapd (802.11ac + EAP-TLS)
- apu-check-updates (Turn on LED 3 when updates are available)
- PKI roles with CA on Ansible host (used for EAP-TLS server and clients)
- tunnelbroker.net endpoint update role
- Bind LEDs of the APU2 to the status of arbitrary systemd units

## TODO

- PKI
  - CRL?
  - Arbitrary CA host?
- systemd LEDs
  - Simplify tasks (apu-led-x vs apu-led-init-x tasks)

## Known Bugs

- named: Still some startup issues
- pki_ca: Using the validity period option exposes a module crash in Ansible 2.7 and earlier. See my
  [bug report][ansibug] and [pull request][ansipull] for details.

[ansibug]: https://github.com/ansible/ansible/issue/47505
[ansipull]: https://github.com/ansible/ansible/pull/47508