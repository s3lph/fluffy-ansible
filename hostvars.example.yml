---
# Example host_vars configuration

# Network configuration
network:
  # Enable IP forwarding
  ip_forward: yes
  # Configuration of network interfaces.
  # Interfaces are matched against using the provided MAC address, and will be renamed to the name given there
  interfaces:
    # Then WAN interface receives its IPv4 address via DHCP, and does not have a routed IPv6 address (thanks to my ISP)
    - name: wan0
      link:
        mac: 02:00:00:00:00:00
        # Start the v6tun interface when starting this interface
        tunnel: v6tun
        # This interface is required for network-online.target
        online_required: yes
      ipv4:
        # Obtain the IPv4 address via DHCP
        dhcp: yes
    # Let's set up a 6in4 tunnel to work around my ISP's neglect regarding IPv6
    - name: v6tun
      link:
        kind: sit
        # IPv4 address of the remote endpoint
        tun_remote: 1.2.3.4
        # Enable regular update of the client address
        tun_dynamic_update: yes
        # This interface is required for network-online.target
        online_required: yes
        # The MTU to use on this link, 1480 because the v6 packet is prepended with a 20 byte v4 header
        mtu: 1480
      # Of course, there's IPv6 on this interface (and no IPv4)
      ipv6:
        addresses:
          # In-tunnel address
          - 2001:db8:1337:42::2/64
        routes:
          # Default route for IPv6
          - prefix: 2000::/3
            gateway: 2001:db8:1337:42::1
            # The tunnel endpoint forwards some other prefixes to us, set the preferred address
            preferred_source: 2001:db8:1337:42::2
    # The wifi0 defice is controlled by hostapd, no configuration is required here
    - name: wifi0
      link:
        mac: 02:00:00:00:00:01
    # The wired LAN interface, will be part of a bridge together with wifi0, so not much here
    - name: lan0
      link:
        mac: 02:00:00:00:00:02
        # The bridge to add this interface to
        bridge: lanbr0
        online_required: yes
    # The lanbr0 bridge is master to the lan0 and wifi0 interfaces (though wifi0 will be added by hostapd)
    - name: lanbr0
      link:
        kind: bridge
        online_required: yes
        # Reduce the MTU on the other interfaces as well, so traffic can pass through the v6 tunnel without being dropped
        mtu: 1480
      ipv4:
        # Configure a static IPv4 address/subnet
        addresses:
          - 192.168.0.1/24
        # Enable and configure the DHCP server
        dhcp_server: yes
        dhcp_range:
          min: 192.168.0.2
          max: 192.168.0.254
        # Configure a domain name
        domain_name: lan.home
      ipv6:
        # Configure a static IPv6 address/subnet
        addresses:
          - 2001:db8:1338:10::1/64
        # Enable router advertisement on this interface
        ra: yes
      # Enable the DNS server on the interface
      dns_server: yes
    # The DMZ interface, config is equivalent to the lanbr0 interface
    - name: dmz0
      link:
        mac: 02:00:00:00:00:03
        online_required: yes
        # Reduce the MTU on the other interfaces as well, so traffic can pass through the v6 tunnel without being dropped
        mtu: 1480
      ipv4:
        addresses:
          - 192.168.1.1/24
        dhcp_server: yes
        dhcp_range:
          min: 192.168.1.2
          max: 192.168.1.254
      ipv6:
        addresses:
          - 2001:db8:1338:11::1/64
        ra: yes
      dns_server: yes

# tunnelbroker.net update configuration.
tunnelbroker_update:
  # The tunnelbroker.net username
  username: test
  # The tunnelbroker.net tunnel ID (available in "IPv6 Tunnel" tab)
  tunnel_id: 0
  # The tunnelbroker.net update key (available in "Advanced" tab)
  secret: please+ignore
  # The systemd OnCalendar string.  Defaults to 'hourly'.  See manpage systemd.time(7)
  # Example: Update every 30 minutes.
  interval: '*-*-* *:0,30:0'

# iptables configuration.  Basically an abstraction of the "normal" iptables config
iptables:
  tables:
    # Configuration for the "filter" table
    - name: filter
      chains:
        # Configuration for the "INPUT" chain
        - name: input
          # Default policy: drop everything
          policy: drop
          rules:
            # Just some regular iptables rules, will be applied to both IPv6 and IPv4
            - rule: -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
            - rule: -i lanbr0 -j ACCEPT
            - rule: -p icmp -j ACCEPT
            - rule: -i dmz0 -p udp -m udp --dport 53 -j ACCEPT
            - rule: -i lo -j ACCEPT
            # Some rules have to be defined differently for IPv6 and IPv4
            - ipv4: -j REJECT --reject-with icmp-admin-prohibited
              ipv6: -j REJECT --reject-with icmp6-adm-prohibited
        # Same for the "FORWARD" chain
        - name: forward
          policy: drop
          rules:
            - rule: -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
            - rule: -i lanbr0 -j ACCEPT
            - rule: -i dmz0 -o wan0 -j ACCEPT
            - rule: -i dmz0 -o v6tun -j ACCEPT
            - rule: -j REJECT --reject-with icmp-admin-prohibited
              ipv6: no
            - rule: -j REJECT --reject-with icmp6-adm-prohibited
              ipv4: no
        # This one's simpler: Just allow every traffic to go out
        - name: output
          policy: accept
    # Configure NAT on the wan0 interface
    - name: nat
      chains:
        - name: postrouting
          rules:
            # Some rules may only have to be applied to IPv4 (or IPv6), but not the other
            - ipv4: -o wan0 -j MASQUERADE

# PKI CA
# THIS SHOULD ONLY BE ON LOCALHOST
pki_ca:
  # Name of the PKI. Should be the same for the CA and entity roles for each PKI
  eap:
    # Base directory for the CA files
    directory: "/etc/ssl/my_ca"
    ca:
      # Passphrase of the CA private key
      passphrase: IThinkIShouldUseAnsibleVaultForThis
      # Validity period of the CA certificate, in seconds
      validity_period: "{{ 10 * 365 * 24 * 3600 }}"
      # Renew the CA certificate if it expires in less than 30 days
      proactive_renewal_period: "{{ 30 * 24 * 3600 }}"
      # CN of the CA certificate
      cn: My CA
      # Parts of the subject that are in the CA cert and in every issued cert
      subject:
        C: Space
        L: ISS
        O: ESA
    # Validity period of issued certificates, in seconds
    issued_validity_period: "{{ 365 * 24 * 3600 }}"
    # Renew the issued certificate if it expires in less than 30 days
    issued_proactive_renewal_period: "{{ 30 * 24 * 3600 }}"

    
# PKI entity: server (or client)
pki_entity:
  # Same name as above
  eap:
    # "server" or "client", corresponds to "serverAuth" and "clientAuth" extendedKeyUsage
    role: server
    # Name of this entity.  Will be used as basename for created files
    name: server
    # Passphrase of the private key, optional
    passphrase: IThinkIShouldUseAnsibleVaultForThis
    # Base directory for created files
    directory: /etc/hostapd/eap
    # Common name of this entity, used in the certificate
    cn: My EAP Server
    # Some systems, e.g. Android, require a PKCS#12 file containing the CA cert and their own cert and key
    # This option will create an additional file named <name>.pfx containing these.
    outform: pkcs12

# hostapd configuration for EAP-TLS
hostapd:
  wifi_interface: wifi0
  bridge_interface: lanbr0
  wifi_driver: nl80211
  ctrl_interface: /var/run/hostapd.sock
  ssid: fluffy
  max_num_sta: 16
  phy_standards:
    - 'n'
    - 'ac'
  # Configuration of EAP-TLS.
  eap:
    message: "Hier könnte Ihre Werbung stehen!"
    tls_user: fluffy
    ca_cert: /etc/hostapd/eap/ca.crt
    dh_file: /etc/hostapd/eap/dh.pem
    server_cert: /etc/hostapd/eap/server.crt
    private_key: /etc/hostapd/eap/server.key

# Configuration binding the APU2's LEDs to a systemd unit.
# This will cause the LED to turn on when the unit starts and off when it stops (or vice versa).
apu_leds:
  # Bind LED 1 to the network-online.target unit.
  led_1:
    unit: network-online.target
  # Bind LED 2 to the hostapd.service unit.
  led_2:
    unit: hostapd.service
    # LED 2 should turn off when hostapd is running and on, when its not.
    inverted: yes
  
